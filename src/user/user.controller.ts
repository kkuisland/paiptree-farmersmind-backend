import { Body, Controller, Param, Post, Query, Get } from '@nestjs/common';
import { Message } from 'src/@types';
import { UserInfo } from './dto/user-info.dto';
import { UserLoginDto } from './dto/user-login.dto';
import { UserDto } from './dto/user.dto';
import { VerifyEmailDto } from './dto/verify-email.dto';
import { UserService } from './user.service';

@Controller('users')
export class UserController {
	constructor(private readonly userService: UserService) {}

	// 회원가입
	@Post()
	async register(@Body() userDto: UserDto): Promise<void> {
		console.log(this.userService.register(userDto));
	}

	// 이메일 인증
	@Post('/email-verify')
	async verifyEmail(@Query() verifyEmailDto: VerifyEmailDto): Promise<string> {
		console.log(verifyEmailDto);
		return verifyEmailDto.signupVerifyToken;
	}

	// 로그인
	@Post('/login')
	async login(@Body() userLoginDto: UserLoginDto): Promise<Message> {
		console.log(userLoginDto);
		return { message: 'success' };
	}

	// 회원정보 조회
	@Get('/:id')
	async getUserInfo(@Param('id') userId: string): Promise<UserInfo> {
		console.log(userId);
		return { name: 'name', email: 'email' };
	}
}
