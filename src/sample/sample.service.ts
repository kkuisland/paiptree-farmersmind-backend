import { Injectable } from '@nestjs/common';
import { CreateSampleDto } from './dto/create-sample.dto';
import { UpdateSampleDto } from './dto/update-sample.dto';

@Injectable()
export class SampleService {
	create(createSampleDto: CreateSampleDto) {
		createSampleDto;
		return 'This action adds a new sample';
	}

	findAll() {
		return `This action returns all sample`;
	}

	findOne(id: number) {
		return `This action returns a #${id} sample`;
	}

	update(id: number, updateSampleDto: UpdateSampleDto) {
		updateSampleDto;
		return `This action updates a #${id} sample`;
	}

	remove(id: number) {
		return `This action removes a #${id} sample`;
	}
}
