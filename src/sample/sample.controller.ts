import {
	Controller,
	Get,
	Post,
	Body,
	Patch,
	Param,
	Delete,
	Req,
	Query,
	HttpCode,
	BadRequestException,
	Header,
	Redirect,
} from '@nestjs/common';
import { SampleService } from './sample.service';
import { CreateSampleDto } from './dto/create-sample.dto';
import { UpdateSampleDto } from './dto/update-sample.dto';
import { Request } from 'express';

@Controller('sample')
export class SampleController {
	constructor(private readonly sampleService: SampleService) {}

	/**
	 * ^기본 라우팅
	 */
	@Get('hello')
	hello() {
		return 'sample hello';
	}

	/**
	 * ^와일드카드
	 * (-, . 문자인식, *외 ?, +, () 와일드 카드와 동일)
	 */
	@Get('wild*card')
	wild() {
		return '와일드 카드';
	}

	/**
	 * ^요청 객체 (Request Object)
	 */
	@Get()
	reqObject(@Req() req: Request): string {
		// @Query(), @Param(key?:string), @Body() 존재
		console.log(req);
		return 'Request Object';
	}

	/**
	 * ^응답 (Response)
	 * POST 201, 그외 200 상태코드 전달
	 */
	@HttpCode(202) // 강제 상태코드 지정
	@Patch(':id')
	patchUpdate(@Param('id') id: string, @Body() updateSampleDto: UpdateSampleDto) {
		+id;
		return updateSampleDto;
	}

	// 400 Bad Request 예외 전달 설정
	@Get(':id')
	findId(@Param('id') id: string): number | never {
		if (+id < 1) {
			throw new BadRequestException('id는 0보다 큰 값이어야 합니다.');
		}
		return +id;
	}

	/**
	 * ^ 헤더(Header)
	 */
	@Header('Custom', 'Test Header')
	@Get(':id')
	findOneWithHeader(@Param('id') id: string) {
		return +id;
	}

	/**
	 * ^ 리디렉셔(Redirection)
	 * 301 Moved Permanatly는 요청한 리소스가 헤더에 주어진 리소스로 완전히 이동됐다는 뜻
	 * 301, 307, 308과 같이 Redirect로 정해진 응답코드가 아닐 경우 브라우저가 제대로 반응하지 않을 수 있습니다.
	 */
	@Redirect('https://nestjs.com', 301)
	@Get('id')
	redirect(@Param('id') id: string) {
		return +id;
	}

	// 동적으로 이동
	@Get('redirect/docs')
	@Redirect('https://docs.nestjs.com', 302)
	getDocs(@Query('version') version: string) {
		if (version && version === '5') {
			return { url: 'https://docs.nestjs.com/v5/', statusCode: 302 };
		}
		return undefined;
	}

	/**
	 * ^ 라우트 파라미터
	 */
	// params의 타입이 any가 되어서 권하지 않는다.
	// 파리미터는 항상 string이기 때문에 명시적으로 {[key:string] : string} 지정
	@Delete(':userId/memo/:memoId')
	deleteUserMemo(@Param() params: { [key: string]: string }) {
		return `userId: ${params.userId}, memoId: ${params.memoId}`;
	}
	// 추천 사용법
	@Delete(':userId/memo/:memoId')
	deleteUserDelMemo(@Param('userId') userId: string, @Param('memoId') memoId: string) {
		return `userId: ${userId}, memoId: ${memoId}`;
	}

	/**
	 * ^ 하위 도메인(Sub-Domain) 라우팅
	 * ! apiSample.controller.ts 파일 참조
	 */

	/**
	 * ^ 페이로드 다루기
	 * export class CreateUserDto {
	 * 	name: string;
	 * 	email: string;
	 * }
	 */
	@Post()
	createTest(@Body() createSampleDto: CreateSampleDto) {
		const { title, contents } = createSampleDto;
		return `샘플 생성 완료 제목: ${title}, 내용: ${contents}`;
	}

	@Get(':title/:contents')
	createGet(@Query() sample: CreateSampleDto) {
		const { title, contents } = sample;
		return `title: ${title}, contents: ${contents}`;
	}

	/* --------- 기본 셋 */
	@Post()
	create(@Body() createSampleDto: CreateSampleDto) {
		return this.sampleService.create(createSampleDto);
	}

	@Get()
	findAll() {
		return this.sampleService.findAll();
	}

	@Get(':id')
	findOne(@Param('id') id: string) {
		return this.sampleService.findOne(+id);
	}

	@Patch(':id')
	update(@Param('id') id: string, @Body() updateSampleDto: UpdateSampleDto) {
		return this.sampleService.update(+id, updateSampleDto);
	}

	@Delete(':id')
	remove(@Param('id') id: string) {
		return this.sampleService.remove(+id);
	}
}
