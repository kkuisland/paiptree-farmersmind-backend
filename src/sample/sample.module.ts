import { Module } from '@nestjs/common';
import { SampleService } from './sample.service';
import { SampleController } from './sample.controller';
import { ApiSampleController } from './apiSample.controller';

@Module({
	controllers: [ApiSampleController, SampleController],
	providers: [SampleService],
})
export class SampleModule {}
