import { Controller, Get, HostParam } from '@nestjs/common';

@Controller({ host: 'api.localhost' })
export class ApiSampleController {
	/**
	 * ^ 하위 도메인(Sub-Domain) 라우팅
	 * http://api.localhost:3000
	 */
	@Get() // 같은 루트 경로
	index(): string {
		return 'Hello, API'; // 다른 응답
	}
}
// 서브 도메인을 변수로 받아 API 버저닝
@Controller({ host: ':version.api.localhost' })
export class ApiSampleVersionController {
	/**
	 * ^ 하위 도메인(Sub-Domain) 버저닝 라우팅
	 * http://v1.api.localhost:3000
	 */
	@Get() // 같은 루트 경로
	index(@HostParam('version') version: string): string {
		return `Hello, API v${version}`; // 다른 응답
	}
}
