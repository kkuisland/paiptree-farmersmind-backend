import { Module } from '@nestjs/common';
import { SampleModule } from './sample/sample.module';
import { UserModule } from './user/user.module';
@Module({
	imports: [SampleModule, UserModule],
	controllers: [],
	providers: [],
})
export class AppModule {}
