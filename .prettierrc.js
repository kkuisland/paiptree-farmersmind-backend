module.exports = {
	semi: true,
	singleQuote: true,
	bracketSpacing: true,
	trailingComma: 'es5',
	arrowParens: 'always',
	useTabs: true,
	tabWidth: 2,
	printWidth: 120,
	endOfLine: 'lf',
	proseWrap: 'preserve',
	htmlWhitespaceSensitivity: 'strict',
	jsxSingleQuote: true,
	jsxBracketSameLine: false,
	quoteProps: 'as-needed',
	vueIndentScriptAndStyle: true,
	requirePragma: false,
	insertPragma: false,
};
